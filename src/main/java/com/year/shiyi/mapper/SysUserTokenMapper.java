package com.year.shiyi.mapper;

import com.baomidou.mybatisplus.extension.api.R;
import com.year.shiyi.common.utils.Result;
import com.year.shiyi.entity.mybatis.SysUserTokenEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 系统用户Token Mapper 接口
 * </p>
 *
 * @author shiyi
 * @since 2021-02-02
 */
@Mapper
public interface SysUserTokenMapper extends BaseMapper<SysUserTokenEntity> {


}
