package com.year.shiyi.mapper;

import com.year.shiyi.entity.mybatis.SysConfigEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 系统配置信息表 Mapper 接口
 * </p>
 *
 * @author shiyi
 * @since 2021-03-04
 */
@Mapper
public interface SysConfigMapper extends BaseMapper<SysConfigEntity> {

}
