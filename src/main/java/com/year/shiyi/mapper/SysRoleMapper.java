package com.year.shiyi.mapper;

import com.year.shiyi.entity.mybatis.SysRoleEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 角色 Mapper 接口
 * </p>
 *
 * @author shiyi
 * @since 2021-02-02
 */
@Mapper
public interface SysRoleMapper extends BaseMapper<SysRoleEntity> {

    /**
     * 根据用户id查询角色信息
     * @param userId 用户id
     * @return role
     */
    List<SysRoleEntity> listRoleByUserId(@Param("userId") Long userId);
}
