package com.year.shiyi.mapper;

import com.year.shiyi.entity.mybatis.SysMenuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 菜单管理 Mapper 接口
 * </p>
 *
 * @author shiyi
 * @since 2021-02-23
 */
@Mapper
public interface SysMenuMapper extends BaseMapper<SysMenuEntity> {

    /**
     * 用户菜单列表
     * @param userId 用户id
     * @return long
     */
    List<Long> queryMenuIdByUserId(@Param("userId") Long userId);

    /**
     * 用户权限
     */
    List<String> queryAllPermsByUserId(@Param("userId") long userId);

    /**
     * 查询用户菜单
     */
    List<SysMenuEntity> queryMenuByUserId(@Param("userId") Long userId);
}
