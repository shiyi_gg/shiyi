package com.year.shiyi.mapper;

import com.year.shiyi.entity.mybatis.SysLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 系统日志 Mapper 接口
 * </p>
 *
 * @author shiyi
 * @since 2021-02-20
 */
@Mapper
public interface SysLogMapper extends BaseMapper<SysLogEntity> {

}
