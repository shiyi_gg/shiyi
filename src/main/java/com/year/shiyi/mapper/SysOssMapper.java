package com.year.shiyi.mapper;

import com.year.shiyi.entity.mybatis.SysOssEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 文件上传 Mapper 接口
 * </p>
 *
 * @author shiyi
 * @since 2021-03-04
 */
@Mapper
public interface SysOssMapper extends BaseMapper<SysOssEntity> {

}
