package com.year.shiyi.mapper;

import com.year.shiyi.entity.mybatis.SysUserEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 系统用户 Mapper 接口
 * </p>
 *
 * @author shiyi
 * @since 2021-02-02
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUserEntity> {

}
