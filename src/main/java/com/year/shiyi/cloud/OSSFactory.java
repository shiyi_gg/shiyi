package com.year.shiyi.cloud;

import com.year.shiyi.common.utils.ConfigConstant;
import com.year.shiyi.common.utils.Constant;
import com.year.shiyi.common.utils.SpringContextUtils;
import com.year.shiyi.service.SysConfigService;

/**
 * @author shiyi on 2021/3/4 15:30
 */
public class OSSFactory {

    private static SysConfigService sysConfigService;

    static {
        OSSFactory.sysConfigService = (SysConfigService) SpringContextUtils.getBean("sysConfigService");
    }

    public static CloudStorageService build() {
        CloudStorageConfig config =  sysConfigService.getConfigObject(ConfigConstant.CLOUD_STORAGE_CONFIG_KEY, CloudStorageConfig.class);

        if (config.getType() == Constant.CloudService.ALIYUN.getValue()) {
            return new AliyunCloudStorageService(config);
        }
        return null;
    }


}
