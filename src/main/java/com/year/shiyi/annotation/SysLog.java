package com.year.shiyi.annotation;

import java.lang.annotation.*;

/**
 * @author shiyi on 2021/2/20 11:07
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SysLog {

    String value() default "";
}
