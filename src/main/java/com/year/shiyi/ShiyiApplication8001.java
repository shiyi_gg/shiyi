package com.year.shiyi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShiyiApplication8001 {

    public static void main(String[] args) {
        SpringApplication.run(ShiyiApplication8001.class, args);
    }
}
