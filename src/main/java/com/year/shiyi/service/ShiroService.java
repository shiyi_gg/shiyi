package com.year.shiyi.service;

import com.year.shiyi.entity.mybatis.SysUserEntity;

import java.util.Set;

/**
 * @author shiyi on 2021/2/23 16:22
 */
public interface ShiroService {

    /**
     * 获取用户权限列表
     * @param userId 用户id
     * @return set
     */
    Set<String> getUserPermissions(long userId);

    /**
     * 根据用户ID，查询用户
     * @param userId 用户id
     * @return user
     */
    SysUserEntity queryUser(Long userId);
}
