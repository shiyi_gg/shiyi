package com.year.shiyi.service;

import com.year.shiyi.common.utils.PageUtils;
import com.year.shiyi.entity.mybatis.SysOssEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 文件上传 服务类
 * </p>
 *
 * @author shiyi
 * @since 2021-03-04
 */
public interface SysOssService extends IService<SysOssEntity> {

    /**
     * 查询文件列表
     */
    PageUtils queryPage(Map<String, Object> params);

    /**
     * 修改配置
     */
    boolean updateValueByKey(String cloudStorageConfigKey, String toJson);
}
