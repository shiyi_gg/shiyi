package com.year.shiyi.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.year.shiyi.common.utils.PageUtils;
import com.year.shiyi.entity.mybatis.SysUserEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 系统用户 服务类
 * </p>
 *
 * @author shiyi
 * @since 2021-02-02
 */
public interface SysUserService extends IService<SysUserEntity> {

    /**
     * 根据用户名查询用户信息
     */
    SysUserEntity queryByUserName(String username);

    /**
     * 用户列表
     */
    PageUtils<IPage<SysUserEntity>> queryPage(Map<String, Object> param);

    /**
     * 修改用户密码
     */
    boolean updatePassword(Long userId, String password, String newPassword);

    /**
     * 查询用户
     */
    boolean getOneByUserName(String username, Long userId);

    /**
     * 删除用户
     */
    boolean deleteBatch(Long[] userIds);

}
