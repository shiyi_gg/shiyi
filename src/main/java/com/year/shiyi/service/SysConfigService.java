package com.year.shiyi.service;

import com.year.shiyi.entity.mybatis.SysConfigEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统配置信息表 服务类
 * </p>
 *
 * @author shiyi
 * @since 2021-03-04
 */
public interface SysConfigService extends IService<SysConfigEntity> {

    /**
     * 查询验证
     */
    boolean getCaptchaStatus();

    /**
     * 根据key，获取value的Object对象
     */
    <T> T getConfigObject(String key, Class<T> clazz);
}
