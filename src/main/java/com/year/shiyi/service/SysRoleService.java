package com.year.shiyi.service;

import com.year.shiyi.common.utils.PageUtils;
import com.year.shiyi.entity.mybatis.SysRoleEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 角色 服务类
 * </p>
 *
 * @author shiyi
 * @since 2021-02-02
 */
public interface SysRoleService extends IService<SysRoleEntity> {

    /**
     * 根据用户id查询角色信息
     * @param userId 用户id
     * @return role
     */
    List<SysRoleEntity> listRoleByUserId(Long userId);

    /**
     * 查询角色列表
     */
    PageUtils queryPage(Map<String, Object> param, Long userId);

    /**
     * 查询角色列表
     */
    List<SysRoleEntity> select(Map<String, Object> param, Long userId);

    /**
     * 保存角色
     */
    boolean saveRole(SysRoleEntity role);

    /**
     * 修改角色
     */
    boolean update(SysRoleEntity role);

    /**
     * 修改角色状态
     */
    boolean disable(Long roleId, Long userId, String action);

    /**
     * 删除角色
     */
    boolean deleteBatch(Long[] roleIds);
}
