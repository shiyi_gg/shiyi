package com.year.shiyi.service;

import com.year.shiyi.entity.mybatis.SysCaptchaEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;

/**
 * <p>
 * 系统验证码 服务类
 * </p>
 *
 * @author shiyi
 * @since 2021-02-02
 */
public interface SysCaptchaService extends IService<SysCaptchaEntity> {

    /**
     * 获取图片验证码
     */
    BufferedImage getCaptcha(HttpServletResponse response, String uuid);

    /**
     * 验证码是否有效
     */
    boolean validate(String uuid, String captcha);
}
