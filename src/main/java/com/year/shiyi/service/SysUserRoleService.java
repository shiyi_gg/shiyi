package com.year.shiyi.service;

import com.year.shiyi.entity.mybatis.SysUserRoleEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户与角色对应关系 服务类
 * </p>
 *
 * @author shiyi
 * @since 2021-03-04
 */
public interface SysUserRoleService extends IService<SysUserRoleEntity> {
}
