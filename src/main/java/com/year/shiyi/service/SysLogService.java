package com.year.shiyi.service;

import com.year.shiyi.common.utils.PageUtils;
import com.year.shiyi.entity.mybatis.SysLogEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 系统日志 服务类
 * </p>
 *
 * @author shiyi
 * @since 2021-02-20
 */
public interface SysLogService extends IService<SysLogEntity> {

    /**
     * 查询日志
     */
    PageUtils queryPage(Map<String, Object> param);
}
