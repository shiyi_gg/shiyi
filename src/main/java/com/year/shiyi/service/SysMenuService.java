package com.year.shiyi.service;

import com.year.shiyi.entity.mybatis.SysMenuEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 菜单管理 服务类
 * </p>
 *
 * @author shiyi
 * @since 2021-02-23
 */
public interface SysMenuService extends IService<SysMenuEntity> {

    /**
     * 导航菜单
     * @param userId 用户id
     * @return entity
     */
    List<SysMenuEntity> getUserMenuList(Long userId);

    /**
     * 根据用户Id查询菜单权限
     * @param userId 用户id
     * @return string
     */
    List<String> queryAllPermsByUserId(long userId);

    /**
     * 查询菜单列表
     */
    List<SysMenuEntity> listMenu(Long userId);

    /**
     * 删除菜单
     */
    void delete(long menuId);



}
