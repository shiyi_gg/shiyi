package com.year.shiyi.service;

import com.year.shiyi.entity.mybatis.SysRoleMenuEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 角色与菜单对应关系 服务类
 * </p>
 *
 * @author shiyi
 * @since 2021-03-03
 */
public interface SysRoleMenuService extends IService<SysRoleMenuEntity> {

    /**
     * 查询角色菜单
     */
    List<Long> queryMenuIdList(Long roleId);

    /**
     * 保存角色菜单
     */
    boolean saveOrUpdate(Long roleId, List<Long> menuIdList);
}
