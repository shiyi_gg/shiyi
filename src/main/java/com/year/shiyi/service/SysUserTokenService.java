package com.year.shiyi.service;

import com.year.shiyi.common.utils.Result;
import com.year.shiyi.entity.mybatis.SysUserTokenEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统用户Token 服务类
 * </p>
 *
 * @author shiyi
 * @since 2021-02-02
 */
public interface SysUserTokenService extends IService<SysUserTokenEntity> {

    /**
     * 生成token
     */
    Result<Object> createToken(long userId);

    /**
     * 修改token值
     */
    Result<Object> logout(long userId);

}
