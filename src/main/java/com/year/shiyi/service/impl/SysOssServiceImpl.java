package com.year.shiyi.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.year.shiyi.common.utils.PageUtils;
import com.year.shiyi.common.utils.Query;
import com.year.shiyi.entity.mybatis.SysOssEntity;
import com.year.shiyi.mapper.SysOssMapper;
import com.year.shiyi.service.SysOssService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * <p>
 * 文件上传 服务实现类
 * </p>
 *
 * @author shiyi
 * @since 2021-03-04
 */
@Service
public class SysOssServiceImpl extends ServiceImpl<SysOssMapper, SysOssEntity> implements SysOssService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SysOssEntity> page = this.page(new Query<SysOssEntity>()
                        .getPage(params, "create_date", true),
                new QueryWrapper<>());
        return new PageUtils(page);
    }


    @Override
    public boolean updateValueByKey(String cloudStorageConfigKey, String toJson) {
        UpdateWrapper<SysOssEntity> wrapper = new UpdateWrapper<>();
        wrapper.eq("param_key", cloudStorageConfigKey);
        wrapper.set("param_value", toJson);
        return this.update(wrapper);
    }
}
