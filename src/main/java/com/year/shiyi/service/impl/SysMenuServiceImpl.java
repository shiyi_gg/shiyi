package com.year.shiyi.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.year.shiyi.common.utils.Constant;
import com.year.shiyi.entity.mybatis.SysMenuEntity;
import com.year.shiyi.entity.mybatis.SysRoleEntity;
import com.year.shiyi.mapper.SysMenuMapper;
import com.year.shiyi.service.SysMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.year.shiyi.service.SysRoleMenuService;
import com.year.shiyi.service.SysRoleService;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 菜单管理 服务实现类
 * </p>
 *
 * @author shiyi
 * @since 2021-02-23
 */
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenuEntity> implements SysMenuService {

    @Autowired
    private SysRoleService sysRoleService;
    @Autowired
    private SysRoleMenuService sysRoleMenuService;


    @Override
    public List<SysMenuEntity> getUserMenuList(Long userId) {
        List<SysRoleEntity> roleEntities = sysRoleService.listRoleByUserId(userId);
        if (roleEntities != null && roleEntities.size() > 0) {
            Optional<SysRoleEntity> entity = roleEntities.stream().filter(x -> x.getRoleName().equals(Constant.SUPER_ADMIN)).findFirst();
            if (entity.isPresent()) {
               // 查询所有菜单信息
                return this.getAllMenuList(null);
            } else {
                // 用户菜单列表
                List<Long> menuIdList = queryMenuIdByUserId(userId);
                return this.getAllMenuList(menuIdList);
            }
        }
        return new ArrayList<>();
    }

    @Override
    public List<String> queryAllPermsByUserId(long userId) {
        return this.baseMapper.queryAllPermsByUserId(userId);
    }

    @Override
    public List<SysMenuEntity> listMenu(Long userId) {
        List<SysRoleEntity> roleEntities = sysRoleService.listRoleByUserId(userId);
        List<SysMenuEntity> entities = new ArrayList<>();
        if (roleEntities != null && roleEntities.size() > 0) {
            Optional<SysRoleEntity> entity = roleEntities.stream().filter(x -> x.getRoleName().equals(Constant.SUPER_ADMIN)).findFirst();
            if (entity.isPresent()) {
                // 查询所有菜单信息
                entities = this.list();
            } else {
                // 用户菜单列表
                entities = this.baseMapper.queryMenuByUserId(userId);
            }
            for (SysMenuEntity menuEntity : entities) {
                SysMenuEntity sysMenuEntity = this.getById(menuEntity.getParentId());
                if (sysMenuEntity != null) {
                    menuEntity.setParentName(sysMenuEntity.getName());
                }
            }
        }
        return entities;
    }

    @Override
    public void delete(long menuId) {
        //删除菜单
        this.removeById(menuId);
        Map<String, Object> map = new HashMap<>();
        map.put("menu_id", menuId);
        sysRoleMenuService.removeByMap(map);
    }

    private List<Long> queryMenuIdByUserId(Long userId) {
        return this.baseMapper.queryMenuIdByUserId(userId);
    }

    private List<SysMenuEntity> getAllMenuList(List<Long> menuIdList) {
        // 查询根菜单
        List<SysMenuEntity> entities = listMenuList(0L, menuIdList);
        // 递归获取子菜单
        getMenuTreeList(entities, menuIdList);

        return entities;
    }


    private List<SysMenuEntity> getMenuTreeList(List<SysMenuEntity> entities, List<Long> menuIdList) {
        List<SysMenuEntity> subMenuList = new ArrayList<>();
        for (SysMenuEntity entity : entities) {
            if (entity.getType() == Constant.MenuType.CATALOG.getValue()) {
                entity.setList(getMenuTreeList(listMenuList(entity.getMenuId(), menuIdList), menuIdList));
            }
            subMenuList.add(entity);
        }
        return subMenuList;

    }

    /**
     * 获取所有菜单列表
     */
    private List<SysMenuEntity> listMenuList(Long parentId, List<Long> menuIdList) {
        List<SysMenuEntity>  menuList = queryListParentId(parentId);
        if (menuIdList == null) {
            return menuList;
        }
        List<SysMenuEntity> userMenuList = new ArrayList<>();
        for (SysMenuEntity menu : menuList) {
            if (menuIdList.contains(menu.getMenuId())) {
                userMenuList.add(menu);
            }
        }
        return userMenuList;
    }

    /**
     * 查询父菜单id查询子菜单
     */
    private List<SysMenuEntity> queryListParentId(Long parentId) {
        QueryWrapper<SysMenuEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("parent_id", parentId);
        wrapper.orderByAsc("order_num");
        return list(wrapper);
    }
}
