package com.year.shiyi.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.year.shiyi.common.utils.PageUtils;
import com.year.shiyi.common.utils.Query;
import com.year.shiyi.entity.mybatis.SysLogEntity;
import com.year.shiyi.mapper.SysLogMapper;
import com.year.shiyi.service.SysLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * <p>
 * 系统日志 服务实现类
 * </p>
 *
 * @author shiyi
 * @since 2021-02-20
 */
@Service
public class SysLogServiceImpl extends ServiceImpl<SysLogMapper, SysLogEntity> implements SysLogService {

    @Override
    public PageUtils queryPage(Map<String, Object> param) {
        String key = (String) param.get("key");
        IPage<SysLogEntity> page = this.page(
                new Query<SysLogEntity>().getPage(param),
                new QueryWrapper<SysLogEntity>()
                    .like(StringUtils.isNotBlank(key), "username", key)
                    .or()
                    .like(StringUtils.isNotBlank(key), "operation", key));
        return new PageUtils(page);
    }
}
