package com.year.shiyi.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.year.shiyi.common.utils.PageUtils;
import com.year.shiyi.common.utils.Query;
import com.year.shiyi.entity.mybatis.SysUserEntity;
import com.year.shiyi.mapper.SysUserMapper;
import com.year.shiyi.service.SysUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;

/**
 * <p>
 * 系统用户 服务实现类
 * </p>
 *
 * @author shiyi
 * @since 2021-02-02
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUserEntity> implements SysUserService {

    @Override
    public SysUserEntity queryByUserName(String username) {
        QueryWrapper<SysUserEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("username", username);
        return getOne(wrapper);
    }

    @Override
    public PageUtils<IPage<SysUserEntity>> queryPage(Map<String, Object> param) {
        IPage<SysUserEntity> page = this.page(
                new Query<SysUserEntity>().getPage(param, true),
                new QueryWrapper<>()
        );
        return new PageUtils<>(page);
    }

    @Override
    public boolean updatePassword(Long userId, String password, String newPassword) {
        SysUserEntity entity = new SysUserEntity();
        entity.setPassword(newPassword);
        entity.setUpdateTime(new Date());
        return this.update(entity, new QueryWrapper<SysUserEntity>().eq("user_id",userId).eq("password", password));
    }

    @Override
    public boolean getOneByUserName(String username, Long userId) {
        return this.getOne(new QueryWrapper<SysUserEntity>()
                .eq("username", username)
                .ne(userId != null, "user_id", userId)
        ) == null;
    }

    @Override
    public boolean deleteBatch(Long[] userIds) {
        return this.removeByIds(Arrays.asList(userIds));
    }
}
