package com.year.shiyi.common.utils;

/**
 * @author shiyi on 2021/3/4 14:25
 */
public class ConfigConstant {

    /**
     * 验证码KEY
     */
    public final static String CAPTCHA_KEY = "CAPTCHA_KEY";

    /**
     * 云存储配置KEY
     */
    public final static String CLOUD_STORAGE_CONFIG_KEY = "CLOUD_STORAGE_CONFIG_KEY";
}
