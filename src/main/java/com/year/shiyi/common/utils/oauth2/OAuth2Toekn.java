package com.year.shiyi.common.utils.oauth2;

import org.apache.shiro.authc.AuthenticationToken;

/**
 * @author shiyi on 2021/2/23 16:11
 */
public class OAuth2Toekn implements AuthenticationToken {

    private String token;

    public OAuth2Toekn(String token) {
        this.token = token;
    }

    @Override
    public Object getPrincipal() {
        return this.token = token;
    }

    @Override
    public Object getCredentials() {
        return token;
    }
}
