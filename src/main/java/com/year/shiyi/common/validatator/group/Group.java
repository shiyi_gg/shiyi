package com.year.shiyi.common.validatator.group;

import javax.validation.GroupSequence;

/**
 * @author shiyi on 2021/3/3 23:48
 */
@GroupSequence({AddGroup.class, UpdateGroup.class})
public interface Group {

}
