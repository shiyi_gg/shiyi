package com.year.shiyi.common.validatator;

import com.year.shiyi.common.exception.BusinessException;
import org.apache.commons.lang.StringUtils;

/**
 * @author shiyi on 2021/3/3 23:48
 */
public class Assert {


    public static void isBlank(String str, String message) {
        if (StringUtils.isBlank(str)) {
            throw new BusinessException(message);
        }
    }

    public static void isNull(Object object, String message) {
        if (object == null) {
            throw new BusinessException(message);
        }
    }
}
