package com.year.shiyi.entity.mybatis;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 系统验证码
 * </p>
 *
 * @author shiyi
 * @since 2021-02-02
 */
@Data
@TableName("sys_captcha")
public class SysCaptchaEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * uuid
     */
    @TableField(value = "uuid")
    private String uuid;

    /**
     * 验证码
     */
    @TableField("code")
    private String code;

    /**
     * 过期时间
     */
    @TableField("expire_time")
    private Date expireTime;
}
