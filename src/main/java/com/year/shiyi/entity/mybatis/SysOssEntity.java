package com.year.shiyi.entity.mybatis;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 文件上传
 * </p>
 *
 * @author shiyi
 * @since 2021-03-04
 */
@Data
@TableName("sys_oss")
public class SysOssEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * URL地址
     */
    @TableField("url")
    private String url;

    /**
     * 文件描述
     */
    @TableField("file_des")
    private String fileDes;

    /**
     * 创建时间
     */
    @TableField("create_date")
    private Date createDate;
}
