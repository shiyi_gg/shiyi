package com.year.shiyi.entity.mybatis;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import com.year.shiyi.common.validatator.group.AddGroup;
import com.year.shiyi.common.validatator.group.UpdateGroup;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 菜单管理
 * </p>
 *
 * @author shiyi
 * @since 2021-02-23
 */
@Data
@TableName("sys_menu")
public class SysMenuEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "menu_id", type = IdType.AUTO)
    private Long menuId;

    /**
     * 父菜单ID，一级菜单为0
     */
    @NotBlank(message = "父级菜单不能为空", groups = {AddGroup.class, UpdateGroup.class})
    @TableField("parent_id")
    private Long parentId;

    /**
     * 菜单名称
     */
    @NotNull(message = "菜单名称不能为空", groups = {AddGroup.class, UpdateGroup.class})
    @TableField("name")
    private String name;

    /**
     * 菜单URL
     */
    @NotNull(message = "菜单URL不能为空", groups = {AddGroup.class, UpdateGroup.class})
    @TableField("url")
    private String url;

    /**
     * 授权(多个用逗号分隔，如：user:list,user:create)
     */
    @TableField("perms")
    private String perms;

    /**
     * 类型   0：目录   1：菜单   2：按钮
     */
    @TableField("type")
    private Integer type;

    /**
     * 菜单图标
     */
    @TableField("icon")
    private String icon;

    /**
     * 排序
     */
    @TableField("order_num")
    private Integer orderNum;

    @TableField(exist = false)
    private Boolean open;

    @TableField(exist = false)
    private List<?> list;

    /**
     * 父菜单名称
     */
    @TableField(exist=false)
    private String parentName;

}
