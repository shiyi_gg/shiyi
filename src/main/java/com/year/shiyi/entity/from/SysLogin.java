package com.year.shiyi.entity.from;

/**
 * @author shiyi on 2021/2/24 16:47
 */
public class SysLogin {

    private String username;
    private String password;
    private String captcha;
    private String uuid;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCaptcha() {
        return captcha;
    }

    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public SysLogin() {
    }

    public SysLogin(String username, String password, String captcha, String uuid) {
        this.username = username;
        this.password = password;
        this.captcha = captcha;
        this.uuid = uuid;
    }
}
