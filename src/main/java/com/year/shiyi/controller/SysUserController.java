package com.year.shiyi.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.year.shiyi.annotation.SysLog;
import com.year.shiyi.common.utils.PageUtils;
import com.year.shiyi.common.utils.Result;
import com.year.shiyi.common.validatator.Assert;
import com.year.shiyi.common.validatator.ValidatorUtils;
import com.year.shiyi.common.validatator.group.AddGroup;
import com.year.shiyi.common.validatator.group.UpdateGroup;
import com.year.shiyi.entity.mybatis.SysUserEntity;
import com.year.shiyi.service.SysUserService;
import com.year.shiyi.service.SysUserTokenService;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.Map;

/**
 * <p>
 * 系统用户 前端控制器
 * </p>
 *
 * @author shiyi
 * @since 2021-02-02
 */
@RestController
@RequestMapping("/sys/user")
public class SysUserController extends AbstractController{

    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private SysUserTokenService sysUserTokenService;

    @GetMapping("/list")
    public Result<Object> list(@RequestParam Map<String, Object> param) {
        PageUtils<IPage<SysUserEntity>> page = sysUserService.queryPage(param);
        return Result.success(page);
    }

    @SysLog("保存用户")
    @PostMapping("/save")
    public Result<Object> save(@RequestBody SysUserEntity user) {
        ValidatorUtils.validateEntity(user, AddGroup.class);
        if (!sysUserService.getOneByUserName(user.getUsername(), null)) {
            return Result.fail("用户名已存在");
        }
        user.setCreateTime(new Date());
        user.setUpdateTime(new Date());
        return sysUserService.save(user) ? Result.success() : Result.fail();
    }

    @SysLog("修改用户")
    @PostMapping("/update")
    public Result<Object> update(@RequestBody SysUserEntity user) {
        ValidatorUtils.validateEntity(user, UpdateGroup.class);
        if (!sysUserService.getOneByUserName(user.getUsername(),user.getUserId())) {
            return Result.fail("用户名已存在");
        }
        user.setUpdateTime(new Date());
        return sysUserService.updateById(user) ? Result.success() : Result.fail();
    }

    /**
     * 删除用户
     */
    @SysLog("删除用户")
    @PostMapping("/delete")
    public Result<Object> delete(@RequestBody Long[] userIds){
        return sysUserService.deleteBatch(userIds) ? Result.success() : Result.fail();
    }

    /**
     * 获取登录的用户信息
     */
    @GetMapping("/info")
    public Result<Object> info(){
        return Result.success(getUser());
    }


    /**
     * 修改用戶登录密码
     */
    @SysLog("修改密码")
    @PostMapping("/password")
    public Result<Object> password(@RequestParam String password, @RequestParam String newPassword){
        Assert.isBlank(password, "旧密码不为能空");
        Assert.isBlank(newPassword, "新密码不为能空");

        //sha256加密
        String oldPassword = new Sha256Hash(password, getUser().getSalt()).toHex();
        //sha256加密
        String newPwd = new Sha256Hash(newPassword, getUser().getSalt()).toHex();

        //更新密码
        return sysUserService.updatePassword(getUserId(), oldPassword, newPassword) ? Result.success() : Result.fail("原密码不正确");

    }

    /**
     * 退出
     */
    @PostMapping("/sys/logout")
    public Result<Object> logout() {
        sysUserTokenService.logout(getUserId());
        return Result.success();
    }
}

