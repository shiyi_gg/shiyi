package com.year.shiyi.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.year.shiyi.annotation.SysLog;
import com.year.shiyi.common.exception.BusinessException;
import com.year.shiyi.common.utils.Constant;
import com.year.shiyi.common.utils.Result;
import com.year.shiyi.common.validatator.ValidatorUtils;
import com.year.shiyi.common.validatator.group.Group;
import com.year.shiyi.entity.mybatis.SysMenuEntity;
import com.year.shiyi.service.ShiroService;
import com.year.shiyi.service.SysMenuService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

/**
 * <p>
 * 菜单管理 前端控制器
 * </p>
 *
 * @author shiyi
 * @since 2021-02-23
 */
@RestController
@RequestMapping("/sys/menu")
public class SysMenuController extends AbstractController{

    @Autowired
    private SysMenuService sysMenuService;

    @Autowired
    private ShiroService shiroService;

    /**
     * 导航菜单
     */
    @GetMapping("/nav")
    public Result<Object> nav(){
        List<SysMenuEntity> menuList = sysMenuService.getUserMenuList(getUserId());
        Set<String> permissions = shiroService.getUserPermissions(getUserId());
//        return Objects.requireNonNull(Result.success().put("menuList", menuList)).put("permissions", permissions);
        return Result.success(menuList);
    }

    @RequiresPermissions(value = "sys:menu:save")
    @SysLog(value = "保存菜单")
    @PostMapping("/save")
    public Result<Object> save(@RequestBody SysMenuEntity entity) {
        ValidatorUtils.validateEntity(entity, Group.class);
        verifyForm(entity);
        sysMenuService.save(entity);
        return Result.success();
    }

    /**
     * 修改
     */
    @SysLog("修改菜单")
    @PostMapping("/update")
    @RequiresPermissions("sys:menu:update")
    public Result<Object> update(@RequestBody SysMenuEntity menu){
        //数据校验
        ValidatorUtils.validateEntity(menu, Group.class);
        verifyForm(menu);
        sysMenuService.updateById(menu);
        return Result.success();
    }

    /**
     * 所有菜单列表
     */
    @GetMapping("/list")
    public Result<Object> list() {
       List<SysMenuEntity> list = sysMenuService.listMenu(getUserId());
       return Result.success(list);
    }

    /**
     * 菜单信息
     */
    @GetMapping("/info/{menuId}")
    @RequiresPermissions("sys:menu:info")
    public Result<Object> info(@PathVariable("menuId") Long menuId){
        SysMenuEntity menu = sysMenuService.getById(menuId);
        return Result.success(menu);
    }

    @SysLog("删除菜单")
    @PostMapping("/delete")
    public Result<Object> delete(@PathVariable("menuId") long menuId){
        SysMenuEntity entity = sysMenuService.getById(menuId);
        if (!(entity == null || !entity.getType().equals(Constant.MenuType.CATALOG))) {
            return Result.fail("系统菜单不能删除");
        }
        List<SysMenuEntity> entities = sysMenuService.list(new QueryWrapper<SysMenuEntity>().eq("parent_id", menuId));
        if (entities.size() > 0) {
            return Result.fail("请先删除子菜单或按钮");
        }
        sysMenuService.delete(menuId);
        return Result.success();
    }

    private void verifyForm(SysMenuEntity entity) {
        // 上级菜单类型
        int parentType = Constant.MenuType.CATALOG.getValue();
        if(entity.getParentId() != 0){
            SysMenuEntity parentMenu = sysMenuService.getById(entity.getParentId());
            parentType = parentMenu.getType();
        }

        // 目录、菜单
        if(entity.getType() == Constant.MenuType.CATALOG.getValue() ||
                entity.getType() == Constant.MenuType.MENU.getValue()){
            if(parentType != Constant.MenuType.CATALOG.getValue()){
                throw new BusinessException("上级菜单只能为目录类型");
            }
            return ;
        }

        // 按钮
        if(entity.getType() == Constant.MenuType.BUTTON.getValue()){
            if(parentType != Constant.MenuType.MENU.getValue()){
                throw new BusinessException("上级菜单只能为菜单类型");
            }
        }
    }

}

