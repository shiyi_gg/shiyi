package com.year.shiyi.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 系统用户Token 前端控制器
 * </p>
 *
 * @author shiyi
 * @since 2021-02-02
 */
@RestController
@RequestMapping("/sys/token/")
public class SysUserTokenController {

}

