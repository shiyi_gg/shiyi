package com.year.shiyi.controller;


import com.year.shiyi.service.SysCaptchaService;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * <p>
 * 系统验证码 前端控制器
 * </p>
 *
 * @author shiyi
 * @since 2021-02-02
 */
@RestController
public class SysCaptchaController {


    @Autowired
    private SysCaptchaService service;

    /**
     * 获取验证码
     *//*
    @GetMapping("captcha.jpg")
    public void captcha(HttpServletResponse response, String uuid) throws IOException {
        BufferedImage image = service.getCaptcha(response, uuid);
        ServletOutputStream out = response.getOutputStream();
        ImageIO.write(image, "jpg", out);
        IOUtils.closeQuietly(out);
    }*/

}

