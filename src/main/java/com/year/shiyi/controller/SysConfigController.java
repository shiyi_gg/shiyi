package com.year.shiyi.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 系统配置信息表 前端控制器
 * </p>
 *
 * @author shiyi
 * @since 2021-03-04
 */
@RestController
@RequestMapping("/sys/config")
public class SysConfigController {

}

