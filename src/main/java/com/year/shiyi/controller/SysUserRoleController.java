package com.year.shiyi.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 用户与角色对应关系 前端控制器
 * </p>
 *
 * @author shiyi
 * @since 2021-03-04
 */
@RestController
@RequestMapping("/sysUserRoleEntity")
public class SysUserRoleController {

}

