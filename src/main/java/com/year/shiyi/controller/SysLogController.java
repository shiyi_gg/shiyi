package com.year.shiyi.controller;


import com.year.shiyi.common.utils.PageUtils;
import com.year.shiyi.common.utils.Result;
import com.year.shiyi.service.SysLogService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * <p>
 * 系统日志 前端控制器
 * </p>
 *
 * @author shiyi
 * @since 2021-02-20
 */
@RestController
@RequestMapping("/sys/log")
public class SysLogController {

    @Autowired
    private SysLogService sysLogService;

    @GetMapping("/list")
    @RequiresPermissions(value = "sys:log:list")
    public Result<Object> list(@RequestParam Map<String, Object> param) {
            PageUtils  page = sysLogService.queryPage(param);
        return Result.success(page);
    }

}

