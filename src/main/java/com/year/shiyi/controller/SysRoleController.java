package com.year.shiyi.controller;


import com.year.shiyi.annotation.SysLog;
import com.year.shiyi.common.utils.PageUtils;
import com.year.shiyi.common.utils.Result;
import com.year.shiyi.common.validatator.ValidatorUtils;
import com.year.shiyi.common.validatator.group.Group;
import com.year.shiyi.entity.mybatis.SysRoleEntity;
import com.year.shiyi.service.SysRoleMenuService;
import com.year.shiyi.service.SysRoleService;
import com.year.shiyi.service.SysUserRoleService;
import com.year.shiyi.service.SysUserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 角色 前端控制器
 * </p>
 *
 * @author shiyi
 * @since 2021-02-02
 */
@RestController
@RequestMapping("/sys/role")
public class SysRoleController extends AbstractController{

    @Autowired
    private SysRoleService service;
    @Autowired
    private SysRoleMenuService sysRoleMenuService;
    @Autowired
    private SysUserRoleService sysUserRoleService;

    /**
     * 查询角色列表
     */
    @GetMapping("/list")
    public Result<Object> list(@RequestBody Map<String, Object> param) {
        PageUtils page = service.queryPage(param,getUserId());
        return Result.success(page);
    }

    /**
     * 查询角色列表
     */
    @GetMapping("/select")
    public Result<Object> select(@RequestBody Map<String, Object> param) {
        List<SysRoleEntity> list = service.select(param, getUserId());
        return Result.success(list);
    }

    /**
     * 角色信息
     */
    @GetMapping("/info/{roleId}")
    public Result<Object> info(@PathVariable("roleId") Long roleId) {
        SysRoleEntity role = service.getById(roleId);
        List<Long> menuIdList = sysRoleMenuService.queryMenuIdList(roleId);
        role.setMenuIdList(menuIdList);
        return Result.success(role);
    }

    /**
     * 保存角色
     */
    @SysLog("保存角色")
    @PostMapping("/save")
    public Result<Object> save(@RequestBody SysRoleEntity role) {
        ValidatorUtils.validateEntity(role, Group.class);
        role.setCreateUserId(getUserId());
        return service.saveRole(role) ? Result.success() : Result.fail();
    }

    /**
     * 修改角色
     */
    @SysLog("修改角色")
    @PostMapping("/update")
    public Result<Object> update(@RequestBody SysRoleEntity role){
        ValidatorUtils.validateEntity(role);
        role.setCreateUserId(getUserId());
        boolean b = service.update(role);
        return Result.success();
    }

    @SysLog("修改角色状态")
    @GetMapping("/upOrDown/{roleId}")
    public Result<Object> upOrDown(@PathVariable Long roleId, @RequestParam("action") String action) {
        return service.disable(roleId, getUserId(), action) ? Result.success() : Result.fail();
    }

    /**
     * 删除角色
     */
    @SysLog("删除角色")
    @PostMapping("/delete")
    @RequiresPermissions("sys:role:delete")
    public Result<Object> delete(@RequestBody Long[] roleIds){
        return service.deleteBatch(roleIds) ? Result.success() : Result.fail();
    }



}

