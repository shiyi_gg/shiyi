package com.year.shiyi.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 角色与菜单对应关系 前端控制器
 * </p>
 *
 * @author shiyi
 * @since 2021-03-03
 */
@RestController
@RequestMapping("/sys/role/menu")
public class SysRoleMenuController {

}

