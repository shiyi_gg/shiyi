package com.year.shiyi.controller;

import com.google.gson.Gson;
import com.year.shiyi.cloud.CloudStorageConfig;
import com.year.shiyi.cloud.OSSFactory;
import com.year.shiyi.common.exception.BusinessException;
import com.year.shiyi.common.utils.ConfigConstant;
import com.year.shiyi.common.utils.Constant;
import com.year.shiyi.common.utils.PageUtils;
import com.year.shiyi.common.utils.Result;
import com.year.shiyi.common.validatator.ValidatorUtils;
import com.year.shiyi.common.validatator.group.AliyunGroup;
import com.year.shiyi.entity.mybatis.SysOssEntity;
import com.year.shiyi.service.SysConfigService;
import com.year.shiyi.service.SysOssService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * 文件上传 前端控制器
 * </p>
 *
 * @author shiyi
 * @since 2021-03-04
 */
@RestController
@RequestMapping("/sys/oss")
public class SysOssController {

    @Autowired
    private SysOssService sysOssService;
    @Autowired
    private SysConfigService sysConfigService;

    @GetMapping("/list")
    public Result<Object> list(@RequestParam Map<String, Object> params) {
        PageUtils page = sysOssService.queryPage(params);
        return Result.success(page);
    }

    /**
     * 云存储配置信息
     */
    @GetMapping("/config")
    public Result<Object> config(){
        CloudStorageConfig config = sysConfigService.getConfigObject(ConfigConstant.CLOUD_STORAGE_CONFIG_KEY, CloudStorageConfig.class);
        return Result.success(config);
    }

    /**
     * 保存云存储配置信息
     */
    @PostMapping("/saveConfig")
    public Result<Object> saveConfig(@RequestBody CloudStorageConfig config){
        //校验类型
        ValidatorUtils.validateEntity(config);
        if (config.getType() == Constant.CloudService.ALIYUN.getValue()) {
            ValidatorUtils.validateEntity(config, AliyunGroup.class);
        }
        boolean b = sysOssService.updateValueByKey(ConfigConstant.CLOUD_STORAGE_CONFIG_KEY, new Gson().toJson(config));
        return b ? Result.success() : Result.fail();
    }

    /**
     * 上传文件
     */
    @PostMapping("/upload")
    public Result<Object> upload(@RequestParam(value = "id",required = false) Long id,
                                 @RequestParam("file") MultipartFile file) throws Exception {
        if (file.isEmpty()) {
            throw new BusinessException("上传文件不能为空");
        }
        // 上传文件
        String suffix = Objects.requireNonNull(file.getOriginalFilename()).substring(file.getOriginalFilename().lastIndexOf("."));
        String url = Objects.requireNonNull(OSSFactory.build()).uploadSuffix(file.getBytes(), suffix);
        // 保存文件信息
        SysOssEntity ossEntity = new SysOssEntity();
        ossEntity.setUrl(url);
        sysOssService.save(ossEntity);
        return Result.success(url);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    public Result<Object> save(@RequestParam("url") String url,@RequestParam(value = "fileDes",required = false) String fileDes){
        SysOssEntity ossEntity = new SysOssEntity();
        ossEntity.setUrl(url);
        ossEntity.setFileDes(fileDes);
        sysOssService.save(ossEntity);
        return Result.success();
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    @RequiresPermissions("sys:oss:all")
    public Result<Object> delete(@RequestBody Long[] ids){
        sysOssService.removeByIds(Arrays.asList(ids));
        return Result.success();
    }

}

